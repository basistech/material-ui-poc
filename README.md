# AC React+Material UI Proof of Concept
## Features
- Sign in with your ACD credentials and use the "Sign out" button to sign out.
- Search for Business Tasks and display the results list.
- Complete localisation of the app with the click of a button, try it out!
- Responsive on desktop browsers, not optimised for mobile, but check [out this article about responsive grids](https://material.io/design/layout/responsive-layout-grid.html)
## Installation
``npm i``

## Running the app locally
Prerequisites: Have a web browser session with CORS disabled, e.g. run this command:
```
"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --disable-web-security --user-data-dir="C:/ChromeDevSession"
```
Then run `npm start`.