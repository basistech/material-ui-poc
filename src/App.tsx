import CssBaseline from "@material-ui/core/CssBaseline"
import Footer from "./components/Footer"
import Header from "./components/Header"
import { Context, useEffect } from "react"
import TaskGrid, { TaskGridData } from "./components/TaskGrid"
import ActiveControlClient from "./server/client"
import React from "react"
import SignIn from "./components/SignIn"
import { LinearProgress, Typography } from "@material-ui/core"
import { useTranslation } from "react-i18next"

export interface AuthState {
  isAuthenticated: boolean
  user: string
  basicAuth: string
}
const initialAuthState: AuthState = {
  isAuthenticated: false,
  user: "",
  basicAuth: "",
}
export interface AuthStateContext {
  state: AuthState
  dispatch: React.Dispatch<any>
}

export interface TaskSearchState {
  isLoading: boolean
  query: string
  data: TaskGridData[]
}
const initialTaskSearchState: TaskSearchState = {
  isLoading: false,
  query: "",
  data: [],
}
export interface TaskSearchStateContext {
  state: TaskSearchState
  dispatch: React.Dispatch<any>
}

// Global auth context for any component that requires it
export const AuthContext: Context<AuthStateContext> = React.createContext(
  {} as AuthStateContext
)

// Global task loading context for any component that requires it
export const TaskSearchContext: Context<TaskSearchStateContext> = React.createContext(
  {} as TaskSearchStateContext
)

const authReducer = (prevState: AuthState, action: any): AuthState => {
  switch (action.type) {
    case "LOGIN":
      localStorage.setItem("user", JSON.stringify(action.username))
      localStorage.setItem("basicAuth", JSON.stringify(action.basicAuth))
      return {
        ...prevState,
        isAuthenticated: true,
        user: action.username,
        basicAuth: action.basicAuth,
      }
    case "LOGOUT":
      localStorage.clear()
      return {
        ...prevState,
        isAuthenticated: false,
        user: "",
        basicAuth: "",
      }
    default:
      return prevState
  }
}

const taskSearchReducer = (
  prevState: TaskSearchState,
  action: any
): TaskSearchState => {
  switch (action.type) {
    case "NEWSEARCH":
      return {
        ...prevState,
        query: action.query,
      }
    case "SEARCHSTARTED":
      return {
        ...prevState,
        isLoading: true,
      }
    case "TASKLISTREADY":
      return {
        ...prevState,
        isLoading: false,
        data: action.data,
      }
    default:
      return prevState
  }
}

export default function App() {
  const [authState, authDispatch] = React.useReducer(
    authReducer,
    initialAuthState
  )
  const [taskSearchState, taskSearchDispatch] = React.useReducer(
    taskSearchReducer,
    initialTaskSearchState
  )
  const [client, setClient] = React.useState<ActiveControlClient | null>(null)
  const { t } = useTranslation()

  useEffect(() => {
    if (authState.isAuthenticated)
      setClient(new ActiveControlClient(authState.basicAuth))
    else setClient(null)
  }, [authState]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const fetchData = async (query: string) => {
      const tasks: TaskGridData[] = await client!.taskList(
        query,
        authState.user
      )
      taskSearchDispatch({ type: "TASKLISTREADY", data: tasks })
    }

    if (authState.isAuthenticated) {
      taskSearchDispatch({ type: "SEARCHSTARTED" })
      fetchData(taskSearchState.query)
    }
  }, [taskSearchState.query]) // eslint-disable-line react-hooks/exhaustive-deps

  let body: JSX.Element

  if (authState.isAuthenticated) {
    if (taskSearchState.isLoading) {
      body = <LinearProgress />
    } else if (taskSearchState.data.length > 0) {
      body = <TaskGrid data={taskSearchState.data} />
    } else {
      body = (
        <Typography
          component="h1"
          variant="h3"
          style={{ paddingTop: "25px", paddingLeft: "25px" }}
        >
          {taskSearchState.query ? t("bodyNoTasksFound") : t("bodyWelcome")}
        </Typography>
      )
    }
  } else {
    body = <SignIn />
  }

  return (
    <React.Fragment>
      <CssBaseline />
      <AuthContext.Provider
        value={{
          state: authState,
          dispatch: authDispatch,
        }}
      >
        <TaskSearchContext.Provider
          value={{
            state: taskSearchState,
            dispatch: taskSearchDispatch,
          }}
        >
          <Header />
          {body}
          <Footer />
        </TaskSearchContext.Provider>
      </AuthContext.Provider>
    </React.Fragment>
  )
}
