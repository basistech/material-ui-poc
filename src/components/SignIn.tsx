import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import Container from "@material-ui/core/Container"
import CssBaseline from "@material-ui/core/CssBaseline"
import { makeStyles } from "@material-ui/core/styles"
import TextField from "@material-ui/core/TextField"
import Typography from "@material-ui/core/Typography"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"
import React from "react"
import { useTranslation } from "react-i18next"
import { AuthContext } from "../App"
import ActiveControlClient from "../server/client"

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}))

interface SignInState {
  username: string
  password: string
  isSubmitting: boolean
  errorMessage: any
}

const initialState: SignInState = {
  username: "",
  password: "",
  isSubmitting: false,
  errorMessage: null,
}

export default function SignIn(props: any) {
  const classes = useStyles()
  const [state, setState] = React.useState<SignInState>(initialState)
  const { dispatch } = React.useContext(AuthContext)
  const { t } = useTranslation()

  const handleFormSubmit = (event: any) => {
    event.preventDefault()
    setState({
      ...state,
      isSubmitting: true,
      errorMessage: null,
    })

    const auth: string = "Basic " + btoa(state.username + ":" + state.password)
    ActiveControlClient.checkLoginData(auth)
      .then(() => {
        dispatch({
          type: "LOGIN",
          username: state.username,
          basicAuth: auth,
        })
      })
      .catch((error: any) => {
        setState({
          ...state,
          isSubmitting: false,
          errorMessage: error.message || error.statusText,
        })
      })
  }

  const handleInputChange = (event: any) => {
    setState({
      ...state,
      [event.target.name]: event.target.value,
    })
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          {t("loginSignIn")}
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleFormSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label={t("loginUsername")}
            name="username"
            autoFocus
            value={state.username}
            onChange={handleInputChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label={t("loginPassword")}
            type="password"
            id="password"
            autoComplete="current-password"
            value={state.password}
            onChange={handleInputChange}
          />
          {state.errorMessage && (
            <span className="form-error">{state.errorMessage}</span>
          )}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={state.isSubmitting}
          >
            {state.isSubmitting ? t("loginLoadingMsg") : t("loginSignIn")}
          </Button>
        </form>
      </div>
    </Container>
  )
}
