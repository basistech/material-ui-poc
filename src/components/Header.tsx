import {
  AppBar,
  Box,
  Button,
  CssBaseline,
  makeStyles,
  MenuItem,
  Select,
  TextField,
  Toolbar,
  Typography
} from "@material-ui/core"
import TranslateIcon from "@material-ui/icons/Translate"
import React, { ReactNode } from "react"
import { useTranslation } from "react-i18next"
import { AuthContext, TaskSearchContext } from "../App"
import i18n from "../i18n"
import logoImg from "../resources/logo.png"

const useStyles: any = makeStyles((theme) => ({
  appBar: {
    backgroundColor: "#022839",
    boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)", // shadow,
    position: "relative",
    borderBottom: `1px solid ${theme.palette.divider}`,
    height: "70px",
    overflow: "hidden",
  },
  toolbarTitle: {
    flexGrow: 1,
    paddingLeft: "1em",
    flex: 1,
  },
  toolbar: {
    flexWrap: "wrap",
    paddingRight: "1em",
  },
  logo: {
    height: "70%",
  },
  searchField: {
    width: "40ch",
    border: "none",
    "& .MuiInputBase-root": {
      background: "#FFFFFF",
    },
  },
  button: {
    backgroundColor: "inherited",
    border: "1px solid transparent",
    color: "#FFFFFF",
    boxShadow: "0 0 20px #2fa2ba",
    height: "100%",
    marginLeft: "3em",
  },
  languageSelect: {
    color: "#FFFFFF",
    marginLeft: "1em",
    height: "100%",
  },
  rightElements: {
    justifyContent: "space-between",
    flexDirection: "row",
    height: "60%",
    width: "auto",
  },
}))

export default function Header(props: any) {
  const [query, setQuery] = React.useState<string>("")
  const { state: authState, dispatch: authDispatch } = React.useContext(
    AuthContext
  )
  const {
    state: taskSearchState,
    dispatch: taskSearchDispatch,
  } = React.useContext(TaskSearchContext)
  const { t } = useTranslation()

  const {
    appBar,
    toolbarTitle,
    toolbar,
    logo,
    searchField,
    button,
    rightElements,
    languageSelect,
  } = useStyles()

  let additionalItems: JSX.Element = <></>
  if (authState.isAuthenticated)
    additionalItems = (
      <Box className={rightElements}>
        <TextField
          className={searchField}
          id="outlined-search"
          placeholder={t("headerSearchPlaceholder")}
          type="search"
          variant="outlined"
          disabled={taskSearchState.isLoading}
          size="small"
          value={query}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            setQuery(event.target.value)
          }
          onKeyPress={(event: React.KeyboardEvent<HTMLDivElement>) => {
            if (event.key === "Enter" && query.length > 3) {
              event.preventDefault()
              taskSearchDispatch({ type: "NEWSEARCH", query: query })
            }
          }}
        />
        <Button
          className={button}
          size="small"
          onClick={() =>
            authDispatch({
              type: "LOGOUT",
            })
          }
        >
          <h3>{t("headerLogoutText", { username: authState.user })}</h3>
        </Button>
      </Box>
    )

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="static" elevation={0} className={appBar}>
        <Toolbar className={toolbar}>
          <img src={logoImg} alt="logo" className={logo} />
          <Typography
            component="h1"
            variant="h4"
            className={toolbarTitle}
            color="inherit"
            noWrap
          >
            ActiveControl
          </Typography>
          {additionalItems}
          <Select
            className={languageSelect}
            displayEmpty
            defaultValue={i18n.language === "de" ? "Deutsch" : "English"}
            renderValue={(value: any): ReactNode => {
              return (
                <h3 style={{ fontWeight: "normal" }}>
                  <TranslateIcon fontSize="inherit" />
                  {" " + value}
                </h3>
              )
            }}
            onChange={(event: React.ChangeEvent<any>) => {
              event.preventDefault()
              i18n.changeLanguage(
                event.target.value === "English" ? "en" : "de"
              )
            }}
          >
            <MenuItem value="English">English</MenuItem>
            <MenuItem value="Deutsch">Deutsch</MenuItem>
          </Select>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  )
}
