import { createMuiTheme, ThemeProvider } from "@material-ui/core"
import {
  DataGrid,
  deDE,
  enUS,
  GridColDef,
  GridRowData,
  Localization,
} from "@material-ui/data-grid"
import { TFunction, useTranslation } from "react-i18next"
import i18n from "../i18n"

const columns = (t: TFunction): GridColDef[] => [
  { field: "taskId", headerName: t("taskgridTaskId"), hide: true },
  { field: "caption", headerName: t("taskgridCaption"), flex: 20 },
  { field: "reference", headerName: t("taskgridReference"), flex: 15 },
  { field: "projectCaption", headerName: t("taskgridProject"), flex: 20 },
  { field: "groupCaption", headerName: t("taskgridGroup"), flex: 15 },
  { field: "typeCaption", headerName: t("taskgridType"), flex: 15 },
  {
    field: "priorityCaption",
    headerName: t("taskgridPriority"),
    flex: 10,
  },
]

export interface TaskGridData extends GridRowData {
  taskId: string
  reference: string
  caption: string
  groupCaption: string
  typeCaption: string
  priorityCaption: string
  projectCaption: string
}

export default function TaskGrid(props: { data: TaskGridData[] }) {
  const { t } = useTranslation()
  let currentLang: Localization
  switch (i18n.language) {
    case "en": {
      currentLang = enUS
      break
    }
    case "de": {
      currentLang = deDE
      break
    }
    default:
      currentLang = enUS
  }

  const theme = createMuiTheme(undefined, currentLang)

  return (
    <div
      style={{
        width: "100%",
      }}
    >
      <ThemeProvider theme={theme}>
        <DataGrid
          rows={props.data}
          getRowId={(row) => row.taskId}
          columns={columns(t)}
          pageSize={10}
          autoHeight
        />
      </ThemeProvider>
    </div>
  )
}
