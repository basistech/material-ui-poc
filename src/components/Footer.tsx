import Link from "@material-ui/core/Link"
import Typography from "@material-ui/core/Typography"
import { useTranslation } from "react-i18next"

export default function Footer(props: any) {
  const { t } = useTranslation()

  return (
    <div>
      <div
        style={{
          display: "block",
          padding: "20px",
          height: "60px",
          width: "100%",
        }}
      />
      <div
        style={{
          backgroundColor: "#c6ced2",
          borderTop: "1px solid #E7E7E7",
          textAlign: "center",
          padding: "20px",
          position: "fixed",
          left: "0",
          bottom: "0",
          height: "60px",
          width: "100%",
        }}
      >
        <Typography variant="body1" align="center" gutterBottom>
          {t("footerMotto")}
          <Link href={t("footerUrl")}>
            Basis Technologies
          </Link>
        </Typography>
      </div>
    </div>
  )
}
