import { ACGroup, ACProject, ACCusttab, ACConfigAll, ACStep, ACTarget } from "./actypes"
import { getWebCall } from "./client"

export class Config {
  private _groups: Map<string, ACGroup>
  private _projects: Map<string, ACProject>
  private _types: Map<string, ACCusttab>

  static async create(authentication: string) {
    const cfg: ACConfigAll = await getWebCall(
      authentication,
      "/BTI/TE_FORM_CONFIGURATION_GET",
      {}
    )
    return new Config(cfg)
  }

  static aggregateSteps = (
    steps: ACStep[],
    root: string,
    targets: ACTarget[]
  ): any => {
    return steps
      .filter((s) => s.TARGET === root)
      .map((step) => {
        const children = Config.aggregateSteps(steps, step.NEXTTARGET, targets)
        const target = targets.find((t) => t.TARGET === step.NEXTTARGET)
        const locations = []
        if (step.HAS_INBOX === "X") locations.push({ code: "I", text: "Inbox" })
        if (target && target.SKIPIMPQUEUE !== "X")
          locations.push({ code: "Q", text: "Import queue" })
        if (step.HAS_TESTQ === "X")
          locations.push({ code: "T", text: "Test queue" })
        if (step.HAS_OUTBOX === "X")
          locations.push({ code: "O", text: "Outbox" })
        return { step, target, children, locations }
      })
  }

  constructor(private config: ACConfigAll) {
    this._groups = new Map(this.groups().map((g) => [g.GROUPID, g]))
    this._projects = new Map(this.projects().map((p) => [p.ID, p]))
    this._types = new Map(this.types().map((t) => [t.ID, t]))
  }

  projects() {
    return this.config.T_PROJECTS
  }
  types() {
    const { T_TYPES, T_TYPE_TXT } = this.config
    return T_TYPES.map((t) => {
      const text = T_TYPE_TXT.filter(
        (tt) =>
          tt.PARENTCL === "TYPE" && tt.PARENTID === t.ID && tt.TEXTID === "TXT"
      )
        .sort((t1, t2) =>
          t1.COUNTER === t2.COUNTER ? 0 : t1.COUNTER < t2.COUNTER ? -1 : 1
        )
        .map((tt) => tt.TEXT)
        .join("")

      return { ...t, text }
    })
  }
  groups() {
    return this.config.T_GROUPS
  }
  decodeGroup(id: string) {
    return this._groups.get(id)
  }
  decodeProject(id: string) {
    return this._projects.get(id)
  }
  decodeType(id: string) {
    return this._types.get(id)
  }
}
