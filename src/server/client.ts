import axios from "axios"
import { TaskGridData } from "../components/TaskGrid"
import { Task } from "./actypes"
import { Config } from "./config"

export default class ActiveControlClient {
  private _config: Config | undefined

  constructor(private authentication: string) {
    this.config(true)
  }

  static async checkLoginData(authentication: string): Promise<any> {
    return getWebCall(authentication, "RFCPING", {})
  }

  private async config(refresh = false): Promise<Config> {
    if (refresh || !this._config) {
      this._config = await Config.create(this.authentication)
    }
    return Promise.resolve(this._config)
  }

  async users() {
    const results = await getWebCall(
      this.authentication,
      "/BTI/TE_FORM_USERS_GET",
      {}
    )
    return { results }
  }

  async taskList(search: string, user: string): Promise<TaskGridData[]> {
    const config = await this.config()
    const results = await getWebCall(
      this.authentication,
      "/BTI/TE_TASK_GETLIST",
      {
        X_SEARCH: { TEXT: search },
        X_SEARCH_FLAGS: {
          TEXT: "X",
          SEARCH_MODE: "3",
        },
        X_NAME: user,
      }
    )
    const tasks =
      results.YT_LIST?.map((task: any) => this.convertTask(task, config)) || []
    return tasks
  }

  convertTask(task: Task, config: Config): TaskGridData {
    return {
      taskId: task.ID,
      reference: task.REFERENCE,
      caption: task.CAPTION,
      groupCaption: config.decodeGroup(task.GROUPID)?.TEXT || "",
      typeCaption: config.decodeType(task.TYPEID)?.CAPTION || "",
      priorityCaption: task.PRIORITY,
      projectCaption: config.decodeProject(task.PROJECTID)?.CAPTION || "",
    }
  }

  async task(taskID: string) {
    const config = await this.config()
    const results = await getWebCall(this.authentication, "/BTI/TE_TASK_GET", {
      X_ID: taskID,
    })
    return {
      ID: results.X_ID,
      testers: results.YT_TESTERS.map((tester: any) => {
        return {
          MANDT: tester.MANDT,
          smtpAddress: tester.SMTP_ADDR,
          systemID: tester.SYSTEMID,
          targetRoleId: tester.TARGETROLEID,
          testerID: tester.TESTERID,
          testerName: tester.TESTERNAME,
        }
      }),
      task: this.convertTask(results.Y_TASK, config),
    }
  }

  async figures() {
    return await getWebCall(
      this.authentication,
      "/BTI/TE_FORM_KEY_FIGURES_GET",
      {
        X_USERID: "",
        X_EXCLUDE_PATH: "",
      }
    )
  }

  private makeDate(date: string, time: string) {
    return new Date(
      parseInt(date.substring(0, 4), 10),
      parseInt(date.substring(4, 6), 10),
      parseInt(date.substring(6, 8), 10),
      parseInt(time.substring(0, 2), 10),
      parseInt(time.substring(2, 4), 10),
      parseInt(time.substring(4, 6), 10)
    )
  }
}

export const getWebCall = async (
  auth: string,
  fName: string,
  params: object
) => {
  const response = await axios.post(
    "http://bti1033.bti.local:8000/sap/zacpoc", // ACD
    {
      function: fName,
      exporting: params,
    },
    {
      headers: {
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        Authorization: auth,
      },
    }
  )
  return response.data
}
