export enum Location {
  I = "I",
  Q = "Q",
  T = "T",
  O = "O",
}

export interface LocationNode {
  text: string
  path: string
  target: string
  location: Location
  count: number
  nodes?: TargetNode[]
}

export interface TargetNode {
  text: string
  id: string
  locked: boolean
  error: boolean
  nodes: LocationNode[]
}

export interface PathNode {
  text: string
  path: string
  nodes: TargetNode[]
}

export interface PathGroup {
  text: string
  nodes: PathNode[]
}
export enum AbapFlag {
  Empty = "",
  X = "X",
}
export enum Trfunction {
  K = "K",
  T = "T",
  W = "W",
}

export enum Trstatus {
  D = "D",
  L = "L",
  R = "R",
}

export interface Request {
  TRKORR: string
  REQTEXT: string
  GROUPID: string
  TYPEID: string
  TRFUNCTION: Trfunction
  TRCATEGORY: string
  TRSTATUS: Trstatus
  CLIENT: string
  REQUESTOR: string
  REQNAME: string
  REQDATE: string
  REQTIME: string
  RELDATE: string
  RELTIME: string
  PATH: string
  HASFORM: AbapFlag
  OWNER: string
  COMPLETED: string
  CLIDEP: AbapFlag
  QUEUED: string
  HIDDEN: string
  LOCKED: string
  CONFLICTS: string
  REFERENCE: string
  UMODES: string
  NOEXPLOGS: string
  EXPORTSTAT: string
  TSTIMPSTAT: string
  LOCSTATUS: string
  MANDT: string
  RETURNCODE: string
  TARSYSTEM: string
  CHANGETYPE: string
  MANUAL_STEP_STATUS: string
  PREV_IMP_RESULT: string
  CONFLICT_STAT: string
  SCI_STAT: string
  RISK_STAT: string
  NUM_OBJECTS: string
  NUM_KEYS: string
  CONF_STAT_RUNID: string
  SCI_STAT_RUNID: string
  RISK_STAT_RUNID: string
  REJECTED: string
  AUTO_APPR_STATUS: string
  ENABLE_DELETE_BUTTON: AbapFlag
  order: number
  grouptxt: string
  typetxt: string
}

export enum ProjectStatus {
  Active = "",
  Inactive = "I",
}

export interface Project {
  ID: string
  CAPTION: string
  REFMASK: string
  STATUS: ProjectStatus
  START_DATE: string
  END_DATE: string
  tasks: Task[]
}

export interface Task {
  ID: string
  CAPTION: string
  REFERENCE: string
  GROUPID: string
  TYPEID: string
  TESTERID: string
  PRIORITY: string
  PROJECTID: string
  LOCKED: AbapFlag
  PATH: string
  STAT_DEPL: string
  STAT_PLAN: string
  STAT_DEPL_MAN: AbapFlag
  STAT_PLAN_MAN: AbapFlag
  TESTING_STATUS: string
  CONFLICT_STAT: string
  SCI_STAT: string
  RISK_STAT: string
  NUM_OBJECTS: string
  NUM_KEYS: string
  CREATION_DATE: string
  CREATION_TIME: string
  CREATEDBY: string
  grouptxt: string
  typetxt: string
  projecttxt: string
  requests: Request[]
}

export interface Queue {
  requests: Request[]
  tasks: Task[]
  projects: Project[]
}

export interface ACConfigMain {
  KEYFIELD: string
  TELICENSE: string
  ALLOWUMODE: AbapFlag
  APPROVEOWN: AbapFlag
  CANRELEASE: AbapFlag
  USEOWNUSER: AbapFlag
  CANDELETE: AbapFlag
  REQUIRETSK: AbapFlag
  CANEDITFRM: AbapFlag
  CFGTSTONLY: AbapFlag
  CTRLPTADD: AbapFlag
  TR_CACHE_ENABLED: AbapFlag
  TR_CACHE_TIMEOUT: AbapFlag
  NO_HIDDEN_CR: AbapFlag
  ANALYSIS_MODE: string
  STAT_PLAN: string
  STAT_DEPL: string
  NONTR_SID: string
  NONTR_SAPTARG: string
  MAINT_MODE: AbapFlag
  IMPORTS_DISABLED: AbapFlag
  NOTIFI_DISABLED: AbapFlag
  CURRENCY_KEY: string
  PASSWORD_APPROVE: AbapFlag
  DELTRBUFF: AbapFlag
  TASKREFPREFIX: string
  TASKREFACTIVE: string
  APPROVE_ANYWAY: AbapFlag
  MANSTEPDEFAULTALLOWPROGRESS: AbapFlag
  NEWMANSTEPWARNALLOWPROGRESS: AbapFlag
  DEF_TYP_GRP_PATH: AbapFlag
  ATTACH_REM_AUTH: AbapFlag
}

export interface ACTocConfig {
  KEYFIELD: string
  CREATELEVEL: string
  SELECTLEVEL: string
  REMOVEONOVERTAKE: AbapFlag
  DELETE_EMPTY: AbapFlag
  AUTO_RELEASE: AbapFlag
  OPEN_NEW_TASKS: AbapFlag
  DEPLOYTYPEID: string
  DEPLOYGROUPID: string
  DEPLOY_TR_TARGET: string
  RELEASE_DEPL_TOC: string
  STOP_TOC_IN_TOC: AbapFlag
}

export interface ACPath {
  PATH: string
  PTEXT: string
  GROUPTEXT: string
  INACTIVE: string
  DO_RELEASE: AbapFlag
  MUSTAPPROV: AbapFlag
  PENDTARGET: AbapFlag
  CONFLCTCHK: AbapFlag
  AUTH_CAT1: string
  AUTH_CAT2: string
  SORT_SEQ: string
}

export interface ACStep {
  PATH: string
  TARGET: string
  NEXTTARGET: string
  HAS_INBOX: AbapFlag
  HAS_TESTQ: AbapFlag
  HAS_OUTBOX: AbapFlag
  STAT_INBOX: string
  STAT_OUTBOX: string
  STAT_QUEUE: string
  STAT_IMPORT: string
}

export interface ACTarget {
  TARGET: string
  SYSID: string
  TTEXT: string
  APPROVEIN: AbapFlag
  APPROVEOUT: AbapFlag
  MINAPPRIN: string
  MINAPPROUT: string
  SORTFIELD: string
  SOURCE: string
  SCHEDULED: string
  SCHEDULEID: string
  HIDDEN: string
  PREDTARGET: string
  ORDERQUEUE: string
  ORDERINBOX: string
  TESTQUEUE: string
  DONTSTOP: AbapFlag
  LOGCMD: string
  LOGCMDRFC: string
  LOGCMD2: string
  LOGCMDRFC2: string
  BACKUP: AbapFlag
  NOSIMPANAL: AbapFlag
  UMODES: string
  REMIMPANAL: string
  MERGEMANDT: string
  CRMERGEREQ: AbapFlag
  MANMERGESAP: AbapFlag
  GROUPTEXT: string
  ROLEID: string
  BYPASSSYSID: AbapFlag
  CTSPLUS: AbapFlag
  IMPORT_METHOD: string
  IMPORT_TIMEOUT: string
  CTS_MERGE_ACTIVE: AbapFlag
  CTS_STARTDATE: string
  CTS_STARTTIME: string
  CTS_PORTNAME: string
  MERGE_TARGET: string
  TASK_LOCK_LOC: string
  RENAME_MERGED: AbapFlag
  CANIMPORT: AbapFlag
  CANAPPRIN: AbapFlag
  CANAPPROUT: AbapFlag
  HASPENDING: AbapFlag
  DEF_MERGE_DEVC: string
  FORCE_DEPENDS: AbapFlag
  MRGALL_ROUT_RSFO: AbapFlag
  CHG_MERGE_OWNER: string
  CON_TARGET: string
  CON_FLAG: AbapFlag
  CON_SHOW: AbapFlag
  SKIPIMPQUEUE: AbapFlag
  AUTO_RELEASE: AbapFlag
  BYPASS_TARGET: AbapFlag
  SORT_SEQ: string
  MERGESIZE: string
  MERGEPATH: string
  MERGETYPE: string
  MERGEGROUP: string
  MERGETASK: string
  MERGE_ONERR: string
  MERGE_REN_STOP: string
  ORCHESTRATED: AbapFlag
  ANALYSEONTEST: AbapFlag
  COPYMANSTEPS: AbapFlag
  COPY_CLIENT_SAVE: AbapFlag
  COPY_CLIENT_REL: AbapFlag
  PARTIAL_TESTING: AbapFlag
  AUTO_APPR_INBOX: AbapFlag
  AUTO_APPR_OUTBOX: AbapFlag
  ADD_QUEUED_SCHED: AbapFlag
  PRESERVE_OWNER_ON_MERGE: AbapFlag
  PARALLEL_IMPORT: AbapFlag
}

export interface ACConfigAll {
  CONFIGURATION: ACConfigMain
  TOC_CONFIGURATION: ACTocConfig
  T_AREA: any[]
  T_AUTHGROUPS: ACAuthgroup[]
  T_BUS_UNIT: any[]
  T_CLIENTS: ACClient[]
  T_COMP: any[]
  T_CUSTTAB: ACCusttab[]
  T_DOC_CATEGORIES: ACDocCategory[]
  T_GROUPS: ACGroup[]
  T_PATHS: ACPath[]
  T_PROJECTS: ACProject[]
  T_SCHEDULES: ACSchedule[]
  T_STEPS: ACStep[]
  T_STTHRUST: any[]
  T_TARGETS: ACTarget[]
  T_TARGET_ROLES: ACTargetRole[]
  T_TARGET_SCHED: any[]
  T_TARG_EXT: ACTargEXT[]
  T_TIMES: ACTime[]
  T_TSK_PRTY: ACTskPrty[]
  T_TYPES: ACCusttab[]
  T_TYPE_TXT: ACTypeTxt[]
  T_USER_ROLES: ACUserRole[]
  T_USER_ROLESUX: ACUserRolesux[]
}

export interface ACAuthgroup {
  AUTH_GROUP: string
  DESCRIPTION: string
}

export interface ACClient {
  TARGET: string
  MANDT: string
}

export interface ACCusttab {
  ID: string
  CAPTION: string
  CLASS: ACClass
  HIDDEN?: AbapFlag
}

export enum ACClass {
  Request = "REQUEST",
  Task = "TASK",
}

export interface ACDocCategory {
  TEXTID: string
  TEXT: string
  STATUS: string
  SORTFIELD: string
  CLASS: ACClass
}

export interface ACGroup {
  GROUPID: string
  TEXT: string
  HIDDEN: AbapFlag
  CLASS: ACClass
}

export interface ACProject {
  ID: string
  CAPTION: string
  REFMASK: string
  STATUS: ACProjectStatus
  START_DATE: string
  END_DATE: string
}

export enum ACProjectStatus {
  Active = "",
  Inactive = "I",
}

export interface ACSchedule {
  SCHEDULEID: string
  STEXT: string
  SUSERID: string
}

export interface ACTargetRole {
  ID: string
  CAPTION: string
  ROLE_FUNCTION: string
}

export interface ACTargEXT {
  SID: string
  TARGET: string
  RFCDEST: string
}

export interface ACTime {
  SCHEDULEID: string
  TAG: string
  TIME: string
}

export interface ACTskPrty {
  PRIORITY: string
  PRIORITY_TEXT: string
}

export interface ACTypeTxt {
  PARENTCL: string
  PARENTID: string
  TEXTID: string
  CHILDID: string
  COUNTER: string
  TEXT: string
}

export interface ACUserRole {
  ROLEID: string
  ROLETEXT: string
}

export interface ACUserRolesux {
  ROLEID: string
  UNAME: string
}
